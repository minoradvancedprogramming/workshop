
function TestConsole(testOuputStream) {
	var testNameInput = $('#testName');
    var testTimeInput = $('#testTime');
    var testMessageInput = $('#testMessage');
	
	var testInputStream = $('#testButton').clickAsObservable()
        .map(function(){
            return MessageCreator.createMessage(
                    testNameInput.val(),
                    testTimeInput.val(),
                    testMessageInput.val()
                );
            }
        );

    new ObservableLogger(testInputStream);
    new ObservableLogger(testOuputStream);

    return testInputStream;
}