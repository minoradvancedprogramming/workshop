/**
 * Retrieves all the messages from a stream and prints them to the screen.
 * @param messageStream The message stream to print on the screen.
 */

function UIOutputWrapper(messageStream) {
    var messageContainer = $(".chat .messages:first");

    var printChatMessage = function(message) {
        $('<div class="message">' +
            '<div class="sender">' +
            message.nick +
            '</div>' +
            '<div class="time">' +
            message.timestamp +
            '</div>' +
            '<div class="text">' +
            message.message +
            '</div>' +
            '<br class="clear" />' +
            '</div>').prependTo(messageContainer);
    }
}