/**
 * Retrieves the chatController application input.
 * @returns {Rx.Observable} The input stream.
 */
function UIInputWrapper() {
    var roomInput = $('#room');
    var messageInput = $('#message');
    var button = $('#button');

    return messageStream;
}