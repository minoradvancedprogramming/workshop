var MessageCreator = {};

MessageCreator.createMessage = function(inputMessage, cls, chatRoom) {
    var message = {}

    message.message = inputMessage;
    message.cls = cls;
    message.room = chatRoom;

    return message;
}

MessageCreator.createJoinMessage = function(cls, chatRoom, nickName) {
    var message = {}

    message.cls = cls;
    message.room = chatRoom;
	message.nick = nickName;

    return message;
}

MessageCreator.createServerMessage = function(_message, nick, timestamp) {
    var message = {}
	
    message.message = _message;
    message.nick = nick;
	message.timestamp = timestamp;

    return message;
}

MessageCreator.fromJSON = function(json) {
    var input = $.parseJSON(json);
    return MessageCreator.createServerMessage(input.message, input.nick, input.timestamp);
}