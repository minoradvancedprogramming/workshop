/**
 * A Rx wrapper around a web socket.
 * @param socketLocation The location of the socket to send the data to.
 * @param outputStream Rx.Subject The stream of data which will be send to the web socket.
 * @returns {Rx.Subject} The socket which receives the data of the web socket.
 */
function WebSocketWrapper(socketLocation, outputStream) {
    /**
     * The input stream received by the web socket.
     * @type {Rx.Subject}
     */
    var inputStream = null;

    /**
     * The websocket to communicate with.
     * @type {WebSocket}
     */
	var webSocket = new WebSocket(socketLocation);

    /**
     * Binds the onOpen function of the web socket.
     */
	webSocket.onopen = function() {
	};

    /**
     * Binds the onMessage function of the web socket.
     */
	webSocket.onmessage = function(message) {
        message = message.data;
	};

    /**
     * Binds the onError function of the web socket.
     */
	webSocket.onerror = function(err) {
	};

    /**
     * Binds the onCompleted function of the web socket.
     */
	webSocket.onclose = function() {
	};

    /**
     * Pushes the output stream to the web socket.
     */
	
	return inputStream;
}