# Welkom op de workshop pagina #

## Opdrachten ##

### Opdracht 1: ###

Maak een reactive implementatie van A = B + C.
Hierbij is het de bedoeling dat A zich automatisch aanpast als er een verandering is gebeurd in B of C.

Je kunt gebruik maken van deze [projecten](https://bitbucket.org/minoradvancedprogramming/workshop/src/a3981c948d43494af8a23ae3371e4293ccb9bbe2/a%2Bb%3Dc/?at=master).

Het "Plain old javascript" project kan gebruikt worden als voorbeeld hoe de applicatie zou werken zonder Reactive Programming.

Het "Reactive extensions" project kan gebruikt worden als beginpunt voor het creëren van een reactive applicatie.

Tip: Maak gebruik van combineLatest!

Hieronder is een voorbeeld te zien van de combineLatest functie.
Observable<T>.combineLatest(Observable<T>, function)

https://github.com/Reactive-Extensions/RxJS/blob/master/doc/api/core/observable.md

### Opdracht 2a ###

Maak een reactive implementatie van de interface van de chat, deze wordt gekoppeld aan de test console. Hierdoor kan er al getest worden.

Tip: Gebruik de zelfde manier van het afvangen van events als bij de A+B=C-opdracht gebruikt is.


### Opdracht 2b ###

Nadat we een werkende interface hebben is het natuurlijk ook van belang dat er met elkaar gechat kan worden. Voor de verbinding worden websockets gebruikt. 

Tip: De koppeling tussen een JavaScript systeem en het Rx gedeelte kan gemaakt worden door middel van Subjects.


### Opdracht 3 ###

Bewerk de zoek applicatie zodat hij de servers van wikipedia niet overbelast.

Het is onnodig om na iedere toetsaanslag aan de server te vragen voor het resultaat.
Tevens is het resultaat van minder dan 2 characters vaak niet van belang. Hierdoor moeten deze er uit gefilterd worden.




## Handige informatie ##

Hieronder staan een aantal onderwerpen.
Deze onderwerpen kunnen handige informatie geven bij het doorlopen van de workshop

## Subject: ##
Een subject in reactive extensions is een type dat de eigenschappen van een observable en een observer heeft. 
Deze subject is een hot observable(http://msdn.microsoft.com/en-us/library/hh242977(v=vs.103).aspx) wat dus inhoudt dat de datastream al loopt of er een observer is of niet.
Een goed voorbeeld hiervan is een mousemove event.

Het gebruik van een subject:
In dit voorbeeld wordt een subject gemaakt, geabonneerd op dit subject via de subscribe functie van rx. Vervolgens wordt dezelfde subject gebruikt om de waardes te sturen naar de observer.      

```
#!javascript

var subject = new Rx.Subject();

var subscription = subject.subscribe(
    function (x) { console.log('onNext: ' + x); },
    function (e) { console.log('onError: ' + e.message); },
    function () { console.log('onCompleted'); });

    subject.onNext(1);
    subject.onNext(2);
    subject.onCompleted();
    subscription.dispose();
```

Zoals hierboven te zien is staan er drie anonieme functies gedefineerd in de subscribe functie. 
Deze functie zal elke keer worden aangeroepen als er op subject een OnNext() functie wordt aangeroepen.
http://msdn.microsoft.com/en-us/library/hh242970(v=vs.103).aspx

## Websockets: ##

### Wat is een websocket? ###

Een websocket is een protocol waarmee door middel van TCP socket een connectie kan worden gemaakt tussen een client en een server.
Deze connectie is een Full-duplex(http://www.webopedia.com/TERM/F/full_duplex.html) connectie waarbij de server en client tegelijk naar elkaar kunnen communiceren.
Het mooie van een websocket is dat de server data kan sturen naar de client zonder dat de client daar om vraagt.

Het afhandelen van de communicatie via een websocket in javascript wordt vooral gedaan via events.


```
#!

WebSocket Events:

Event	Event Handler	        Description
open	Socket.onopen		Dit event gebeurt als er een socket connectie is gemaakt.
message	Socket.onmessage	Dit event gebeurt als er data binnekomt van de server.
error	Socket.onerror		Dit event gebeurt als er een error optreedt in de communicatie met de server. 
close	Socket.onclose		Dit event gebeurt als de connectie is gesloten.
```


Het versturen gaat niet via een event maar via de methode aanroep socket.send(dataDatMoetWordenVerstuurd).
Lijst met websocket mogelijkheden:
(http://www.tutorialspoint.com/html5/html5_websocket.htm)