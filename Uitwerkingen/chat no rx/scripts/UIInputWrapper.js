/**
 * Retrieves the chatController application input.
 */
function UIInputWrapper(inputMessageReceiver) {
    var roomInput = $('#room');
    var messageInput = $('#message');
    var button = $('#button');

    button.on("click", function() {
        inputMessageReceiver(MessageCreator.createMessage(
            messageInput.val(),
            "msg",
            roomInput.val()
        ));
    });
}