$(function() {
    chatController(websocketURL);
});

function chatController(websocketURL) {
    new UIInputWrapper(inputMessageReceiver);
    new RoomConsole(chatRoomReceiver);

    var UIOutput= UIOutputWrapper();

    var webSocket = new WebSocketWrapper(
        websocketURL,
        websocketMessageReceiver
    );

    function chatRoomReceiver(message) {
        webSocket.sendMessage(message);
    }

    function inputMessageReceiver(message) {
        webSocket.sendMessage(message);
    }
    function websocketMessageReceiver(message) {
        UIOutput.displayMessage(message);
    }
}