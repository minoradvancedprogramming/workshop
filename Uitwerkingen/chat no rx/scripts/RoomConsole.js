
function RoomConsole(roomCreatorCallback) {
    var roomInput = $('#room');
	var nickInput = $('#userName');
	var button = $('#roomButton');

	button.on("click", function(){
            roomCreatorCallback (MessageCreator.createJoinMessage(
                "join",
                roomInput.val(),
                nickInput.val()
            ));
        }
    );
}