$(function() {
    chatController(websocketURL);
});

function chatController(websocketURL) {
    var UIInputStream = new UIInputWrapper();
    var testConsoleInputStream = new RoomConsole(UIInputStream);
    var socketInputStream = new WebSocketWrapper(
  websocketURL, 
  Rx.Observable.merge(UIInputStream, testConsoleInputStream)
 );

    var UIOutputStream = Rx.Observable.merge(socketInputStream);
    new UIOutputWrapper(UIOutputStream);
	new ObservableLogger(Rx.Observable.merge(UIInputStream, testConsoleInputStream));
}