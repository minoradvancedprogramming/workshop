
function RoomConsole() {
    var roomInput = $('#room');
	var nickInput = $('#userName');
	var button = $('#roomButton');
	
	var rootInputStream = button.clickAsObservable()
        .map(function(){
                return MessageCreator.createJoinMessage(
                    "join",
                    roomInput.val(),
                    nickInput.val()
                );
            }
        );

    return rootInputStream;
}