/**
 * Retrieves the chatController application input.
 * @returns {Rx.Observable} The input stream.
 */
function UIInputWrapper() {
    var roomInput = $('#room');
    var messageInput = $('#message');
    var button = $('#button');

    var date = new Date();

    var messageStream = button.clickAsObservable()
        .map(function(){
                return MessageCreator.createMessage(
                    messageInput.val(),
                    "msg",
                    roomInput.val()
                );
            }
        );

    return messageStream;
}