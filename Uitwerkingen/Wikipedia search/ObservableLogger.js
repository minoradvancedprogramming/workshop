/**
 * Small and simple logger for a Rx observable.
 * @param Rx.observable The observable to log the messages for.
 */


function ObservableLogger(observable) {
    observable.subscribe(
        function(message){
            console.log("Message: " + JSON.stringify(message));
        },
        function(error) {
            console.log("Error: " + error);
        },
        function() {
            console.log("Completed");
        }
    );
}