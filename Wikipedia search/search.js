$(function() {
    var $input = $('#input');

});

function printResult(data) {
    var $results = $('#results')

    $results.empty();
    $.each(data, function (_, value) {
        $('<li>' + value + '</li>').appendTo($results);
    });
}

function printError(error) {
    var $results = $('#results')
    $results.empty();

    $('<li>Error: ' + error + '</li>').appendTo($results);

}

function searchWikipedia (term) {
    return $.ajax({
        url: 'http://nl.wikipedia.org/w/api.php',
        dataType: 'jsonp',
        data: {
            action: 'opensearch',
            format: 'json',
            search: term
        }
    }).promise();
}